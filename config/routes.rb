Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }, except: %i[create update], path: '', path_names: { sign_in: 'login', sign_out: 'signed-out' }
  resources :posts
  resources :users do
    get 'profile'
  end
  resources :posts, only: %i[create update edit show destroy]
  root to: 'posts#index'
end
