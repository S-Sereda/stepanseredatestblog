class UsersController < ApplicationController
  def profile
    @user = User.find_by(id: params[:user_id]) or render status: :not_found

    @user_posts = @user.posts
  end
end
