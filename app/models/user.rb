class User < ApplicationRecord
  has_many :posts, dependent: :destroy
  validates_presence_of %i[first_name last_name nickname email]

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end
