module ApplicationHelper
  def owner?(resource)
    current_user == resource&.user
  end

  def owner(resource)
    resource&.user
  end
end
